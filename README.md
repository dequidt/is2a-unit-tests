# TP Tests Unitaires IS2A4 

Ce dépôt `GitLab` contient un exemple de projet `C` pour lequel la compilation, et la génération de tests... ont été automatisées. Le code métier de ce projet se base sur les algorithmes de tris vus en programmation au S5. Il sert juste à fournir une base de code de taille limitée et simple pour laquelle vous allez implémenter des tests unitaires (_i.e._ tester chacune des fonctions).

## Contenu du dépôt

- `README.md` le fichier que vous être en train de lire
- `Makefile` permet la compilation du code métier (cible `main`) et des tests unitaires (cible `tests`)
- `.clang-format` contient les règles de formatage de code
- le répertoire `includes` contient les déclarations de fonctions
- le répertoire `src` contient les sources du projet en particulier
    + `utils.c`: qui contient des fonctions auxiliaires utiles pour les 2 autres fichiers `.c`
    + `sort_algorithms.c` contient les 3 algorithmes de tri _classiques_: tri par selection, tri par insertion, tri par fusion
    + `main,c`: qui initialise un tableau aléatoire de taille fixée, qui le tri suivant les 3 méthodes et qui compare que les tableaux triés sont bien identiques.
- le répertoire `tests` contient le fichier `.c` qui permet de réaliser les tests unitaires, c'est ce fichier que vous devrez compléter.

## Le fichier `Makefile`

Un fichier `Makefile` assez classique mais pour lequel il a fallu ajouter des options de compilation `-fprofile-arcs -ftest-coverage` et d'édition de liens `--coverage` ce qui va permettre de déterminer la couverture des tests unitaires.

Pour les tests unitaires, il est important d'ajouter la bibliothèque `check` et ses dépendences `rt` et `subunit`

## Le fichier `unit_tests.c`

Il se base sur la bibliothèque [check](https://libcheck.github.io/check/). Le principe est de définir des tests unitaires sur le principe suivant:

```c
START_TEST (nom_du_test)
{
    // déclaration et initialisation de variables
    // appel de la fonction à tester
    // test du résultat de la fonction avec ck_assert
}
END_TEST
```

Ces tests font ensuite partie d'une suite de tests

```c
Suite *nom_suite (void)
{
    Suite *s       = suite_create ("Nom de la suite");
    TCase *tc_core = tcase_create ("Core");

    tcase_add_test (tc_core, nom_du_test);
    // ajout d'autres tests unitaires
    //...

    suite_add_tcase (s, tc_core);
    return s;
}
```

qui peut être lancée par un `runner`

```c
int main (void)
{
    int      no_failed = 0;
    Suite *  s         = nom_suite ();
    SRunner *runner    = srunner_create (s);
    srunner_run_all (runner, CK_NORMAL);
    no_failed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
```

et qui a l'exécution va indiquer le nombre de tests réussis ou échoués.

Au niveau des tests, vous allez devoir écrire pour chacune des fonctions (sauf pour la fonction `main`) 1 ou plusieurs tests en fonction de ce que vous estimez être pertinent. Par exemple pour la fonction `init_rn_generator` définie dans `src/utils.c` il est possible d'écrire le test suivant:

```c
START_TEST (test_rn) 
{ 
    ck_assert_int_eq (init_rn_generator, 1); 
}
END_TEST
```

qui permet de s'assurer que la valeur de retour de la fonction est forcément `1`. D'autres fonctions de comparaison existent comme:

- `ck_assert_int_ne`: teste si deux entiers sont différents
- `ck_assert_int_lt`: teste si un entier est inférieur à un deuxième
- `ck_assert_ptr_null`: teste si un pointer est `NULL`
- `ck_assert_mem_eq`: teste si deux zones mémoires (tableaux) sont identiques
- ...

La liste complète des comparaisons est disponible ici: [https://libcheck.github.io/check/doc/check_html/check_4.html](https://libcheck.github.io/check/doc/check_html/check_4.html)

## Exécution de programme et des tests

Avant même d'écrire des tests vous pouvez compiler le programme et vérifier le premier tests en faisant:

1. `make main`: permet de compiler le programme principal
1. `make tests`: permet de compiler le programme qui va lancer les tests unitaires
1. `./build/tri_comp`: pour exécuter le programme principal
1. `./build/tri_tests`: pour exécuter le programme de tests

Normalement en exécutant `./build/tri_tests` vous devriez avoir l'affichage suivant:

```rtf
Running suite(s): SortAlgorithms
100%: Checks: 1, Failures: 0, Errors: 0
```

qui indique qu'un test a été effectué et qu'il a été réussi.

## Travail à faire

Vous devez écrire des tests unitaires pour les fonctions suivantes:

- `init_array`
- `swap_value_in_array`
- `selection_sort`
- `insertion_sort`
- (`merge`)
- (`recursive_merge`)
- `merge_sort`

Une fois que cela est fait, vérifier que tous les tests fonctionnent. Modifier votre code source en introduisant une erreur et vérifier que cela met en échec certains tests.

## Dernière étape

En général pour s'assurer que le nombre de tests est suffisant, on va exécuter le programme de tests et vérifier que toutes les fonctions du code source sont testées, il s'agit de la couverture de code. Pour cela vous allez taper les commandes suivantes (ou mieux ajouter une cible dans votre `Makefile`):

1. `llvm-cov-11 gcov -f -b build/*`: permet de spécifier où trouver les fichiers compilés avec les informations de couverture de code
1. `lcov --directory build --base-directory . -c -o cov.info`: permet de créer le fichier de couverture de tests
1. `genhtml cov.info`: permet de générer un rapport su format `HTML`, vous pouvez ensuite ouvrir le fichier `index.html` et vérifier que vos tests couvrent bien 100% de votre code... si ça n'est pas le cas, il faut écrire des tests pour les fonctions non couvertes.